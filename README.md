关于 mq-redis-client
==================

本项目是一个基于redis实现的简单多进程消息队列客户端，提供消息发送，消息处理，可以定义处理消息的子进程数量.
发送的消息包括普通消息，优先级消息，未来消息，不支持订阅模式，每个消息有且只有一个客户端进程可以消费.
仅支持Linux环境

## 安装
```php
    composer require zw2582/mq-redis-client
```

## 特点

- 多进程并发执行
- 防丢失和复盘恢复
- 异常队列查看
- 稳定，具有子进程自启能力
- 简单，启动服务和发布消息都只有几段代码

## 使用方法

- 启动客户端服务
config.php由自己定义，可以不传则是默认定义，定义的参数在当前文件夹下config.php中
config.php中有一个handle参数，该参数用于定义自定义的任务处理程序
任务处理程序需要实现 \RedisMqClient\model\TaskHandleInterface 接口

```
$config = require './config.php';

//启动客户端,$config非必填
$client = new RedisMqClient\MQClient($config);
$client->start();
```

- 发送消息
消息可以是字符串，数字，数组，可被json_encode的对象

```
//发送消息
$client->sendTask('new message');

//发送优先级队列
$client->sendTask('new message', 1);

//发送未来执行队列
$client->sendTask('new message', time()+10);   //未来10秒后执行
```

- config配置项

```
[
    //队列配置
    'queue'=>'queue',   //任务队列名称，list
    'queue_future'=>'queue_future', //未来级队列名称,zset
    'queue_ing'=>'queue_ing',   //正在进行队列和复盘队列名称，set
    'queue_exception'=>'queue_exception',   //异常队列名称,list
    
    //最大进程数配置
    'max_task_count'=>5,
    
    //redis相关配置
    'redis'=>[
        'host'=>'127.0.0.1',
        'port'=>'6379',
        'username'=>'',
        'password'=>'',
        'db'=>1
    ],
    
    //任务处理类
    'handle'=>'Mynamespace\MyhandleClass'
]
```

## 其他
有任何其他的问题和建议可以联系:2582308253@qq.com
微信:weishen520123
