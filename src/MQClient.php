<?php
namespace RedisMqClient;

use RedisMqClient\model\TaskBaseHandle;
use RedisMqClient\model\Task;
use RedisMqClient\process\TaskObtainProcess;
use RedisMqClient\process\TaskFutureSyncProcess;
use RedisMqClient\model\TaskHandleInterface;

class MQClient
{
    public $queue = 'queue';
    
    public $queue_future = 'queue_future';
    
    public $queue_ing = 'queue_ing';
    
    public $queue_exception = 'queue_exception';
    
    public $redis_config = [
        'host'=>'127.0.0.1',
        'port'=>'6379',
        'username'=>'',
        'password'=>'',
        'db'=>1
    ];
    
    public $handle = TaskBaseHandle::class; //任务处理对象
    
    public $max_task_count = 5;
    
    private $processArr = [];
    
    private $redis = null;
    
    public function __construct($config = null) {
        $this->initConfig($config);
    }
    
    public function start()
    {
        /*
         * 启动子进程
         * 0号进程用于同步未来队列
         * 其他进程用于执行队列处理任务
         */
        for ($i=0; $i<$this->max_task_count; $i++) {
            $this->forkProcess($i);
        }
        
        do {
            //获取退出的进程序号
            $pid = pcntl_wait($status);
            
            if ($pid != -1) {
                $index = $this->processArr[$pid];
                unset($this->processArr[$pid]);
                //启动该序号进程
                $this->forkProcess($index);
            }
            
            sleep(1);
        } while(true);
    }
    
    /**
     * 开启子进程
     * @param int $index 子进程序号
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:57:34
     */
    private function forkProcess($index) {
        $pid = pcntl_fork();
        if ($pid == -1) {
            echo 'fork进程失败';
        } elseif ($pid) {
            $this->processArr[$pid] = $index;
        } else {
            if ($index == 0) {
                //0号进程用于同步未来队列
                $process = new TaskFutureSyncProcess($this);
                $process->start();
            } else {
                //其他进程用于执行队列处理任务
                $process = new TaskObtainProcess($this);
                $process->start();
            }
        }
    }
    
    /**
     * 初始化配置
     * 
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午9:49:36
     */
    private function initConfig($config) {
        $config['queue'] && $this->queue = $config['queue'];
        $config['queue_future'] && $this->queue_future = $config['queue_future'];
        $config['queue_ing'] && $this->queue_ing = $config['queue_ing'];
        $config['queue_exception'] && $this->queue_exception = $config['queue_exception'];
        $config['redis'] && $this->redis_config = $config['redis'];
        $config['handle'] && $this->handle = $config['handle'];
        
        $refl = new \ReflectionClass($this->handle);
        if (!in_array(TaskHandleInterface::class, $refl->getInterfaceNames())) {
            throw new \Exception('handle必须实现TaskHandleInterface::class接口');
        }
        
        $this->getRedis();
    }
    
    /**
     * 发送数据到任务
     * @param mixed $data
     * @param int $time
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:39:40
     */
    public function sendTask($data, $time = null) {
        $task = new Task($data, $time);
        $this->sendTaskToQueue($task);
        
        return $task;
    }
        
    /**
     * 发送任务
     * @param Task $task
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:39:16
     */
    public function sendTaskToQueue(Task $task) {
        $time = $task->getTime();
        if ($time > time()) {
            //大于当前时间的进未来队列
            $this->sendTaskToFutureQueue($task, $time);
        } elseif ($time >= 0 && $time <= 10){
            //时间定义在0-10之间的进优先级队列
            $this->sendTaskToPriorityQueue($task, $time);
        } else {
            //其他的为正常队列
            $this->sendTaskToNormalQueue($task);
        }
    }
    
    /**
     * 插入正常消息队列
     * @param Task $task
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:36:01
     */
    private function sendTaskToNormalQueue(Task $task) {
        $this->getRedis()->lPush($this->queue, json_encode($task));
    }
    
    /**
     * 
     * @param Task $task
     * @param int $priority
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 下午1:30:38
     */
    private function sendTaskToPriorityQueue(Task $task, int $priority) {
        $this->getRedis()->rPush($this->queue, json_encode($task));
    }
    
    /**
     * 插入未来消息队列
     * @param Task $task
     * @param int $time
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:36:20
     */
    private function sendTaskToFutureQueue(Task $task, int $time) {
        $this->getRedis()->zAdd($this->queue_future, $time, json_encode($task));
    }
    
    /**
     * 获取redis实例
     * @throws \Exception
     * @return \Redis
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午11:31:33
     */
    public function getRedis() {
        if (!$this->redis instanceof \Redis) {
            $this->redis = $this->buildRedisConn($this->redis_config);
        }
        return $this->redis;
    }
    
    /**
     * 连接redis
     * @param array $config
     * @throws \Exception
     * @return \Redis
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午2:53:33
     */
    public function buildRedisConn($config) {
        $redis = new \Redis();
        
        if (!$redis->connect($config['host'], $config['port'])) {
            throw new \Exception('链接redis数据库失败');
        }
        if ($config['db']) {
            $redis->select($config['db']);
        }
        
        return $redis;
    }
    
}

