<?php
return [
    //队列配置
    'queue'=>'queue',   //任务队列，list
    'queue_future'=>'queue_future', //未来级队列,zset
    'queue_ing'=>'queue_ing',   //正在进行队列，set
    'queue_exception'=>'queue_exception',   //异常队列,list
    
    //最大任务数配置
    'max_task_count'=>5,
    
    //redis相关配置
    'redis'=>[
        'host'=>'127.0.0.1',
        'port'=>'6379',
        'username'=>'',
        'password'=>'',
        'db'=>1
    ],
    
    //任务处理类
    'handle'=>''
];