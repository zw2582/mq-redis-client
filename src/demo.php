<?php

use RedisMqClient\MQClient;

$config = require './config.php';
//启动客户端
$client = new MQClient($config);
$client->start();

//发送消息
$client->sendTask('new message');

//发送优先级队列
$client->sendTask('new message', 1);

//发送未来执行队列
$client->sendTask('new message', time()+10);   //未来10秒后执行