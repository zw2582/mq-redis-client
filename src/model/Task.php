<?php
namespace RedisMqClient\model;

/**
 * 定义任务类
 * 
 * @author zhouwei@shzhanmeng.com
 * @copyright 2018年8月8日 下午5:57:15
 */
class Task implements \JsonSerializable
{
    private $id;
    
    private $data;
    
    private $time;
    
    public function __construct($data, $time=null, $id=null) {
        $this->data = $data;
        $this->time = $time ? : time();
        $this->id = $id ?: time().rand(1000,9999);
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getTime() {
        return $this->time;
    }
    
    public function getData() {
        return $this->data;
    }
    
    public function jsonSerialize()
    {
        return [
            'id'=>$this->id,
            'data'=>$this->data,
            'time'=>$this->time
        ];
    }
    
    public function __toString() {
        return json_encode($this->jsonSerialize(), JSON_UNESCAPED_UNICODE);
    }
    
}

