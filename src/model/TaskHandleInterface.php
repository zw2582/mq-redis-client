<?php
namespace RedisMqClient\model;

interface TaskHandleInterface
{
    //任务处理对象的执行入口
    public function handle(Task $task);
}

