<?php
namespace RedisMqClient\process;

use RedisMqClient\MQClient;

/**
 * 未来队列同步到任务队列
 * 
 * @author zhouwei@shzhanmeng.com
 * @copyright 2018年8月9日 上午9:37:52
 */
class TaskFutureSyncProcess extends BaseProcess
{
    /**
     * @var MQClient
     */
    private $client;
    
    private $redis;
    
    public function __construct(MQClient $client) {
        $this->client = $client;
        $this->redis = $client->buildRedisConn($client->redis_config);
    }
    
    public function start()
    {
        $syncQueueing = false;
        $begin = true;
        do {
            //同步未来队列
            $this->syncFuture();
            
            //捡漏处理
            if (in_array(date('H:i'), ['24:00']) || $begin) {
                $begin = false;
                if (!$syncQueueing) {
                    $this->syncQueueing();
                    $syncQueueing = true;
                }
            } elseif ($syncQueueing) {
                $syncQueueing = false;
            }
            
            sleep(1);
        } while (true);
    }
    
    /**
     * 同步未来队列
     * 
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 下午1:05:03
     */
    public function syncFuture() {
        $res = $this->redis->zRangeByScore($this->client->queue_future, 0, time());
        if ($res) {
            foreach($res as $taskData) {
                //未来处理：从队列右侧插入
                $this->redis->lPush($this->client->queue, $taskData);
                //删除原数据
                $this->redis->zRem($this->client->queue_future, $taskData);
            }
        }
    }
    
    /**
     * 同步正在处理任务进程，让其进未来队列，延迟10秒执行
     *
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月8日 下午6:28:48
     */
    public function syncQueueing() {
        $res = $this->redis->sMembers($this->client->queue_ing);
        if ($res) {
            foreach($res as $taskData) {
                //捡漏处理：进未来队列，延迟10秒执行
                $this->redis->zAdd($this->client->queue_future, time()+10, $taskData);
                //删除原数据
                $this->redis->srem($this->client->queue_ing, $taskData);
            }
        }
    }

}

