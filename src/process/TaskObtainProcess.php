<?php
namespace RedisMqClient\process;

use RedisMqClient\MQClient;
use RedisMqClient\model\Task;

/**
 * 获取任务并执行进程
 * 
 * @author zhouwei@shzhanmeng.com
 * @copyright 2018年8月9日 下午12:43:12
 */
class TaskObtainProcess extends BaseProcess
{
    /**
     * @var MQClient
     */
    private $client;
    
    private $redis;
    
    public function __construct(MQClient $client) {
        $this->client = $client;
        $this->redis = $client->buildRedisConn($client->redis_config);
    }
    
    public function start()
    {
        $client = $this->client;
        //获取处理类
        $handleClass = $client->handle;
        $class = new \ReflectionClass($handleClass);
       
        do {
            //获取任务
            $task = $this->obtainTask();
            
            if ($task) {
                /**
                 * 获取任务处理类实例
                 * @var TaskHandleInterface $handle
                 */
                $handleObj = $class->newInstance();
                $this->handleExec($handleObj, $task);
            }
        } while (true);
    }
    
    /**
     * 执行任务
     * @param object $handleObj 执行任务的对象
     * @param object $task 任务对象
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午2:29:19
     */
    public function handleExec($handleObj, $task) {
        try {
            $handleObj->handle($task);
        } catch (\Exception $e) {
            $this->addExceptionQueue($task);
        } finally {
            $this->cleanQueueIng($task);
        }
    }
    
    /**
     * 获取任务去执行
     * 
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 上午9:48:27
     */
    public function obtainTask() {
        $redis = $this->redis;
        $res = $redis->brPop($this->client->queue, 60);
        if ($res && isset($res[1]) && $res[1]) {
            $res = $res[1];
            $redis->sAdd($this->client->queue_ing, $res);
            $res = json_decode($res, true);
            $task = new Task($res['data'], $res['time'], $res['id']);
            return $task;
        }
        return null;
    }
    
    /**
     * 去除正在执行的任务
     * @param Task $task
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 下午12:38:54
     */
    public function cleanQueueIng($task) {
        $redis = $this->redis;
        
        $redis->srem($this->client->queue_ing, json_encode($task));
    }
    
    /**
     * 加入异常队列
     * @param Task $task
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月9日 下午12:40:19
     */
    public function addExceptionQueue($task) {
        $redis = $this->redis;
        
        $redis->lPush($this->client->queue_exception, json_encode($task));
    }
}

