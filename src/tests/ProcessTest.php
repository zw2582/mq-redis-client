<?php
namespace RedisMqClient\tests;

use PHPUnit\Framework\TestCase;
use RedisMqClient\MQClient;
use RedisMqClient\process\TaskObtainProcess;
use RedisMqClient\process\TaskFutureSyncProcess;

class ProcessTest extends TestCase
{
    
    private static $client;
    
    public static function setUpBeforeClass() {
        self::$client = new MQClient();
    }
    /**
     * 测试获取队列
     * 
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午1:00:19
     */
    public function testObtainTask() {
        $client = self::$client;
        $obtain = new TaskObtainProcess($client);
        
        //发送队列
        $client->sendTask("abc");
        
        //获取队列
        $task = $obtain->obtainTask();
        $this->assertNotNull($task, "获取数据不对");
        
        //正在处理队列
        $result = $client->getRedis()->sMembers($client->queue_ing);
        $this->assertTrue(in_array(json_encode($task), $result), '没有进入queue_ing队列');
        
        //清理队列
        $obtain->cleanQueueIng($task);
        $result = $client->getRedis()->sMembers($client->queue_ing);
        $this->assertTrue(!in_array(json_encode($task), $result), '清理queue_ing队列失败');
        
        //加入异常队列
        $obtain->addExceptionQueue($task);
        $result = $client->getRedis()->lPop($client->queue_exception);
        $this->assertEquals(json_encode($task), $result, '加入queue_execption队列失败');
    }
    
    /**
     * 测试未来同步队列
     * 
     * @depends testObtainTask
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午1:52:14
     */
    public function testSyncFuture() {
        $client = self::$client;
        $syncFuture = new TaskFutureSyncProcess($client);
        
        //发送两个未来队列
        $client->getRedis()->zAdd($client->queue_future, 1, 'a1');
        $client->getRedis()->zAdd($client->queue_future, 2, 'a2');
        
        //同步未来队列
        $syncFuture->syncFuture();
        
        //判断未来对垒是否清除
        $count = $client->getRedis()->zCount($client->queue_future, 1, 2);
        $this->assertEquals(0, $count, '未来队列没有清除');
        
        //判断queue是否已同步到数据
        $taskA2 = $client->getRedis()->lPop($client->queue);
        $this->assertTrue('a2' == $taskA2, '获取到的数据不对');
        
        $taskA1 = $client->getRedis()->lPop($client->queue);
        $this->assertTrue('a1' == $taskA1, '获取到的数据不对');
    }
    
    /**
     * 测试复盘进程异常导致终止的任务
     * 
     * @depends testSyncFuture
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午2:03:18
     */
    public function testSyncQueueing() {
        $client = self::$client;
        $syncFuture = new TaskFutureSyncProcess($client);
        
        //丢入两个进程终止任务
        $client->getRedis()->sAdd($client->queue_ing, 'a1');
        $client->getRedis()->sAdd($client->queue_ing, 'a2');
        
        //同步
        $syncFuture->syncQueueing();
        
        //判断queue_future是否已同步到数据
        $data = $client->getRedis()->zRangeByScore($client->queue_future, time()-1, time()+10);
        $data = array_intersect($data, ['a1', 'a2']);
        sort($data);
        $this->assertTrue($data == ['a1', 'a2'], 'queue没有同步到数据');
        
        //queue_ing队列是否已清除
        $count = $client->getRedis()->scard($client->queue_ing);
        $this->assertEquals(0, $count, 'queue_ing队列没有清除');
    }
    
    /**
     * 测试任务处理
     * 
     * @depends testSyncQueueing
     * @author zhouwei@shzhanmeng.com
     * @copyright 2018年8月10日 下午2:31:54
     */
    public function testHandle() {
        $client = self::$client;
        $obtain = new TaskObtainProcess($client);
        
        //发送队列
        $task = $client->sendTask("abc+".time());
        
        //执行任务
        $handleClass = $client->handle;
        $class = new \ReflectionClass($handleClass);
        $handleObj = $class->newInstance();
        $obtain->handleExec($handleObj, $task);
        
        $this->assertTrue(true);
    }
    
}

