<?php
namespace RedisMqClient\tests;

use PHPUnit\Framework\TestCase;
use RedisMqClient\MQClient;

class SendTaskTest extends TestCase
{
    private static $client;
    
    public static function setUpBeforeClass() {
        self::$client = new MQClient();
    }
    
    public function testSendTask() {
        $client = self::$client;
        
        //发送普通消息
        $task = $client->sendTask("abc".time());
        
        $data = $client->getRedis()->lPop($client->queue);
        $this->assertEquals($task->getData(), json_decode($data, true)['data'], '没有塞入队列');
        
        //发送优先级队列
        $task = $client->sendTask("abc".time(), 1);
        
        $data = $client->getRedis()->rPop($client->queue);
        $this->assertEquals($task->getData(), json_decode($data, true)['data'], '没有塞入队列');
        
        //发送未来队列
        $future = time()+20;
        $task = $client->sendTask("abc".time(), $future);
        
        $datas = $client->getRedis()->zRangeByScore($client->queue_future, time(), $future);
        $this->assertTrue(in_array(json_encode($task), $datas), '没有塞入队列');
    }
    
}

