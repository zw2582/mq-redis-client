<?php
require_once  __DIR__.'/../MQClient.php';
require_once __DIR__.'/../process/ProcessInterface.php';
require_once __DIR__.'/../process/BaseProcess.php';
require_once __DIR__.'/../process/TaskObtainProcess.php';
require_once __DIR__.'/../process/TaskFutureSyncProcess.php';
require_once __DIR__.'/../model/TaskHandleInterface.php';
require_once __DIR__.'/../model/TaskBaseHandle.php';
require_once __DIR__.'/../model/Task.php';

